console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database").pool;
const queries = require("../../src/dao/queries");

chai.should();
chai.use(chaiHttp);
const correctUser = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email123@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

const missingFirstNameUser = {
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email123@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

const missingEmailUser = {
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

const missingPasswordUser = {
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email123@gmail.com",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

let id;

describe("Authentication", () => {
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_USER, (err, rows, fields) => {
      if (err) {
        console.log(`beforeEach CLEAR error: ${err}`);
        done(err);
      } else {
        done();
      }
    });
  });

  describe("UC101 Registration", () => {
    it("TC-101-1 should throw an error when a field is missing", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(missingFirstNameUser)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .an("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: firstName must be a string."
            );
          done();
        });
    });

    it("TC-101-2 should throw an error when no email is provided", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(missingEmailUser)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .an("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: emailAdress must be a string."
            );
          done();
        });
    });

    it("TC-101-3 should throw an error when no password is provided", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(missingPasswordUser)
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .an("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: password must be a string."
            );
          done();
        });
    });

    it("TC-101-5 should return a token when providing valid information", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");

          const response = res.body;
          console.log("response: ", response);
          response.should.have.property("id").which.is.a("number");
          response.should.have.property("firstName").which.is.a("string");
          response.should.have.property("lastName").which.is.a("string");
          response.should.have.property("emailAdress").which.is.a("string");
          response.should.have.property("password").which.is.a("string");
          response.should.have.property("phoneNumber").which.is.a("string");
          response.should.have.property("roles").which.is.a("string");
          response.should.have.property("street").which.is.a("string");
          response.should.have.property("city").which.is.a("string");
          response.should.have.property("Token").which.is.a("string");
          done();
        });
    });

    it("TC-101-4 user already exists", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          chai
            .request(server)
            .post("/api/auth/")
            .send(correctUser)
            .end((err, res) => {
              res.should.have.status(400);
              res.body.should.be
                .an("object")
                .that.has.all.keys("datetime", "message");
              let { message } = res.body;
              message.should.be
                .an("string")
                .that.equals("This email has already been taken.");
              done();
            });
        });
    });
  });

  describe("UC102 Login", () => {
    it("TC-102-1 should throw an error when a field is missing", (done) => {
      chai
        .request(server)
        .post("/api/auth/login")
        .send({
          password: "secret"
        })
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .a("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: emailAdress must be a string."
            );

          done();
        });
    });
    it("TC-102-2 should throw an error when email is missing", (done) => {
      chai
        .request(server)
        .post("/api/auth/login")
        .send({
          password: "secret"
        })
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .a("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: emailAdress must be a string."
            );

          done();
        });
    });
    it("TC-102-3 should throw an error when password is missing", (done) => {
      chai
        .request(server)
        .post("/api/auth/login")
        .send({
          emailAdress: "test@test.nl"
        })
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be
            .a("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals(
              "AssertionError [ERR_ASSERTION]: password must be a string."
            );

          done();
        });
    });
    it("TC-102-4 should throw an error when a user doesn't exist", (done) => {
      chai
        .request(server)
        .post("/api/auth/login")
        .send({
          emailAdress: "test1@test.nl",
          password: "secret"
        })
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be
            .a("object")
            .that.has.all.keys("datetime", "message");
          let { message } = res.body;
          message.should.be
            .an("string")
            .that.equals("User not found or password invalid");

          done();
        });
    });
    it("TC-102-5 should return a token when providing valid information", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          chai
            .request(server)
            .post("/api/auth/login")
            .send({
              emailAdress: "email123@gmail.com",
              password: "password123"
            })
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              const response = res.body;
              response.should.have.property("id").which.is.a("number");
              response.should.have.property("firstName").which.is.a("string");
              response.should.have.property("lastName").which.is.a("string");
              response.should.have.property("emailAdress").which.is.a("string");
              response.should.have.property("phoneNumber").which.is.a("string");
              response.should.have.property("roles").which.is.a("string");
              response.should.have.property("street").which.is.a("string");
              response.should.have.property("city").which.is.a("string");
              response.should.have.property("Token").which.is.a("string");
              done();
            });
        });
    });
  });
});
