const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database").pool;
const queries = require("../../src/dao/queries");

chai.should();
chai.use(chaiHttp);
const correctUser = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};
const correctUser1 = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email1@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

let id;

describe("User", () => {
  beforeEach((done) => {
    beforeEach((done) => {
      pool.query(queries.CLEAR_DB_PARTICIPATION, (err, res) => {
        if (err) {
          done(err);
        }
        done();
      });
    });
    pool.query(queries.CLEAR_DB_MEAL, (err, res) => {
      if (err) {
        console.log(err);
        done(err);
      }
      done();
    });
  });
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_USER, (err, res) => {
      if (err) {
        done(err);
      }
      done();
    });
  });
  describe("UC202 Get users", () => {
    it("TC-202-1 should return 0 users", (done) => {
      chai
        .request(server)
        .get("/api/user/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          res.body.result.length.should.be.eql(0);
          done();
        });
    });
    it("TC-202-2 should return 2 users", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          chai
            .request(server)
            .post("/api/auth/")
            .send(correctUser1)
            .end((err, res) => {
              chai
                .request(server)
                .get("/api/user/")
                .end((err, res) => {
                  res.should.have.status(200);
                  res.body.result.should.be.a("array");
                  res.body.result.length.should.be.eql(2);
                  done();
                });
            });
        });
    });
    it("TC-202-3 should return users with non-existent name", (done) => {
      chai
        .request(server)
        .get("/api/user/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          done();
        });
    });
    it("TC-202-4 should return active users", (done) => {
      chai
        .request(server)
        .get("/api/user/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          done();
        });
    });
    it("TC-202-5 should return inactive users", (done) => {
      chai
        .request(server)
        .get("/api/user/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          done();
        });
    });
    it("TC-202-6 should return users with certain name", (done) => {
      chai
        .request(server)
        .get("/api/user/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          done();
        });
    });
  });
  describe("UC203 Get own user data", () => {
    it("TC-203-1 should return error, user not authorized", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          chai
            .request(server)
            .get("/api/user/own/" + id)
            .auth("userToken", { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(401);
              const obj = res.body;
              obj.should.have.property("error");
              obj.error.should.eql("Not authorized");
              done();
            });
        });
    });

    it("TC-203-2 should return a user", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .get("/api/user/own/" + id)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const user = res.body.result;
              user.should.be.a("object");
              user.should.have.property("firstName");
              user.should.have.property("lastName");
              user.should.have.property("emailAdress");
              user.should.have.property("phoneNumber");
              user.should.have.property("roles");
              user.should.have.property("street");
              user.should.have.property("city");
              done();
            });
        });
    });
  });
  describe("UC204 Get user data", () => {
    it("TC-204-1 should return error, user not authorized", (done) => {
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          chai
            .request(server)
            .get("/api/user/own/" + id)
            .auth("userToken", { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(401);
              const obj = res.body;
              obj.should.have.property("error");
              obj.error.should.eql("Not authorized");
              done();
            });
        });
    });

    it("TC-204-2 should return error, invalid user id", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .get("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              obj.should.have.property("status");
              done();
            });
        });
    });

    it("TC-204-3 should return a user", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .get("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const user = res.body.result;
              user.should.be.a("object");
              user.should.have.property("firstName");
              user.should.have.property("lastName");
              user.should.have.property("emailAdress");
              user.should.have.property("phoneNumber");
              user.should.have.property("roles");
              user.should.have.property("street");
              user.should.have.property("city");
              done();
            });
        });
    });
  });

  describe("UC205 Update user data", () => {
    it("TC-205-1 should return error, missing email", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .put("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .send({
              firstName: "John",
              lastName: "Doe",
              phoneNumber: "123456789",
              password: "password123",
              street: "street1",
              city: "city1",
              roles: ""
            })
            .end((err, res) => {
              res.should.have.status(400);
              const obj = res.body;
              obj.should.have.property("message");
              obj.message.should.eql(
                "AssertionError [ERR_ASSERTION]: emailAdress must be a string."
              );
              done();
            });
        });
    });
    it("TC-205-3 should return error, missing phoneNumber", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .put("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .send({
              firstName: "John",
              lastName: "Doe",
              emailAdress: "email@gmail.com",
              password: "password123",
              street: "street1",
              city: "city1",
              roles: ""
            })
            .end((err, res) => {
              res.should.have.status(400);
              const obj = res.body;
              obj.should.have.property("message");
              obj.message.should.eql(
                "AssertionError [ERR_ASSERTION]: phoneNumber must be a string."
              );
              done();
            });
        });
    });
    it("TC-205-4 should return error, user doenst exit", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = Number(res.body.id);
          userToken = res.body.Token;
          chai
            .request(server)
            .put("/api/user/" + 10000000)
            .auth(userToken, { type: "bearer" })
            .send({
              firstName: "John",
              lastName: "Doe",
              emailAdress: "email@gmail.com",
              password: "password123",
              phoneNumber: "123456789",
              street: "street1",
              city: "city1",
              roles: "student"
            })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              done();
            });
        });
    });
    it("TC-205-5 should return error, not authorized", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .put("/api/user/" + id)
            .auth("userToken", { type: "bearer" })
            .send({
              firstName: "John",
              lastName: "Doe",
              emailAdress: "email@gmail.com",
              password: "password123",
              phoneNumber: "123456789",
              street: "street1",
              city: "city1",
              roles: "student"
            })
            .end((err, res) => {
              res.should.have.status(401);
              const obj = res.body;
              obj.should.have.property("error");
              obj.error.should.eql("Not authorized");
              done();
            });
        });
    });
    it("TC-205-6 should update user", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .put("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .send({
              firstName: "John",
              lastName: "Doe",
              emailAdress: "email@gmail.com",
              password: "password123",
              phoneNumber: "123456789",
              street: "street2",
              city: "city2",
              roles: "student"
            })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              obj.should.have.property("status");
              obj.status.should.eql("success");
              done();
            });
        });
    });
  });
  describe("UC206 Delete user", () => {
    it("TC-206-1 should return error, user doesn't exist", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .delete("/api/user/" + 10000000)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              obj.should.have.property("status");
              obj.status.should.eql("success");
              done();
            });
        });
    });
    it("TC-206-2 should return error, not authorized", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .delete("/api/user/" + id)
            .auth("userToken", { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(401);
              const obj = res.body;
              obj.should.have.property("error");
              obj.error.should.eql("Not authorized");
              done();
            });
        });
    });
    it("TC-206-3 should return error, user isnt owner", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .delete("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              obj.should.have.property("status");
              obj.status.should.eql("success");
              done();
            });
        });
    });
    it("TC-206-4 should delete user", (done) => {
      let userToken;
      chai
        .request(server)
        .post("/api/auth/")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .delete("/api/user/" + id)
            .auth(userToken, { type: "bearer" })
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body;
              obj.should.have.property("status");
              obj.status.should.eql("success");
              done();
            });
        });
    });
  });
});