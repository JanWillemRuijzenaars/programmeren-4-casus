const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database").pool;
const queries = require("../../src/dao/queries");

chai.should();
chai.use(chaiHttp);
const correctUser = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email2@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

const correctMeal = {
  id: 1,
  isActive: 1,
  isVega: 0,
  isVegan: 0,
  isToTakeHome: 0,
  dateTime: "2020-01-01 00:00:00",
  maxAmountOfParticipants: 4,
  price: 10,
  imageUrl: "https://www.google.com/",
  cookId: 1,
  createDate: "2020-01-01 00:00:00",
  updateDate: "2020-01-01 00:00:00",
  name: "Test Meal",
  description: "Test Meal Description",
  allergenes: "gluten"
};

const missingFieldMeal = {
  id: 1,
  isActive: 1,
  isVega: 0,
  isVegan: 0,
  isToTakeHome: 0,
  dateTime: "2022-03-22 17:35:00",
  maxAmountOfParticipants: 4,
  price: 10,
  imageUrl: "https://www.google.com/",
  cookId: 1,
  createDate: "2020-01-01 00:00:00",
  updateDate: "2020-01-01 00:00:00",
  description: "Test Meal Description",
  allergenes: "gluten"
};

describe("Meal", () => {
  beforeEach((done) => {
    beforeEach((done) => {
      pool.query(queries.CLEAR_DB_PARTICIPATION, (err, res) => {
        if (err) {
          done(err);
        }
        done();
      });
    });
    pool.query(queries.CLEAR_DB_MEAL, (err, res) => {
      if (err) {
        console.log(err);
        done(err);
      }
      done();
    });
  });
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_USER, (err, res) => {
      if (err) {
        done(err);
      }
      done();
    });
  });


  describe("UC301 create meal", () => {
    it("TC301-1 should return error, missing field", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(missingFieldMeal)
            .end((err, res) => {
              res.should.have.status(422);
              const obj = res.body;
              obj.should.be.a("object");
              obj.should.have.property("error");
              obj.error.should.be.a("string");
              obj.error.should.equal(
                "AssertionError [ERR_ASSERTION]: name must be a string."
              );
              done();
            });
        });
    });
    it("TC301-2 should return error, not authorized", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          chai
            .request(server)
            .post("/api/meal")
            .auth("userToken", { type: "bearer" })
            .send(correctMeal)
            .end((err, res) => {
              res.should.have.status(401);
              const obj = res.body;
              obj.should.be.a("object");
              obj.should.have.property("error");
              obj.error.should.be.a("string");
              obj.error.should.equal("Not authorized");
              done();
            });
        });
    });
    it("TC301-3 should add meal", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              res.should.have.status(200);
              const obj = res.body.result;
              console.log(obj);
              obj.should.be.a("object");
              obj.should.have.property("insertId");
              done();
            });
        });
    });
  });
  describe("UC302 update meal", () => {
    it("TC302-1 should return error, missing field", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;

          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              chai
                .request(server)
                .put("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .send(missingFieldMeal)
                .end((err, res) => {
                  res.should.have.status(422);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal(
                    "AssertionError [ERR_ASSERTION]: name must be a string."
                  );
                  done();
                });
            });
        });
    });
    it("TC302-2 should return error, not authorized", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              chai
                .request(server)
                .put("/api/meal/" + mealId)
                .auth("userToken", { type: "bearer" })
                .send(correctMeal)
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("TC302-3 should return error, not the owner of data", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .put("/api/meal/" + mealId)
                .auth("userToken", { type: "bearer" })
                .send(meal)
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("TC302-4 should return error, meal doesnt exist", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .put("/api/meal")
                .auth(userToken, { type: "bearer" })
                .send(meal)
                .end((err, res) => {
                  res.should.have.status(404);
                  const obj = res.body;
                  obj.should.be.a("object");
                  done();
                });
            });
        });
    });
    it("TC302-5 should update and return meal", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .put("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .send(meal)
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("result");
                  obj.result.should.be.a("object");
                  obj.result.should.have.property("affectedRows");
                  obj.result.affectedRows.should.be.a("number");
                  obj.result.affectedRows.should.equal(1);
                  done();
                });
            });
        });
    });
  });
  describe("UC303 getall meals", () => {
    it("TC303-1 should return list of meals", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .get("/api/meal")
                .auth(userToken, { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("result");
                  obj.result.should.be.a("array");
                  obj.result.length.should.be.a("number");
                  obj.result.length.should.equal(1);
                  done();
                });
            });
        });
    });
  });
  describe("UC304 get meal by id", () => {
    it("TC304-1 should error, meal doesnt exist", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .get("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  done();
                });
            });
        });
    });
    it("TC304-2 should return meal", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .get("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("result");
                  obj.result.should.be.a("object");
                  obj.result.should.have.property("id");
                  obj.result.id.should.be.a("number");
                  obj.result.id.should.equal(mealId);
                  done();
                });
            });
        });
    });
  });
  describe("UC305 delete meal", () => {
    it("TC305-2 should return error, not authorized", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .delete("/api/meal/" + mealId)
                .auth("userToken", { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("TC305-3 should return error, not the owner of data", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .delete("/api/meal/" + mealId)
                .auth("userToken", { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("TC305-4 should return error, meal doesnt exist", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .delete("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  done();
                });
            });
        });
    });
    it("TC305-5 should delete meal", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              console.log(mealId);
              chai
                .request(server)
                .delete("/api/meal/" + mealId)
                .auth(userToken, { type: "bearer" })
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.status.should.eql("success");
                  obj.result.affectedRows.should.eql(1);
                  done();
                });
            });
        });
    });
  });
});
