const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database").pool;
const queries = require("../../src/dao/queries");

chai.should();
chai.use(chaiHttp);
const correctUser = {
  id: 1,
  firstName: "John",
  lastName: "Doe",
  isActive: 1,
  emailAdress: "email2@gmail.com",
  password: "password123",
  phoneNumber: "123456789",
  roles: "",
  street: "street1",
  city: "city1"
};

const correctMeal = {
  id: 1,
  isActive: 1,
  isVega: 0,
  isVegan: 0,
  isToTakeHome: 0,
  dateTime: "2020-01-01 00:00:00",
  maxAmountOfParticipants: 4,
  price: 10,
  imageUrl: "https://www.google.com/",
  cookId: 1,
  createDate: "2020-01-01 00:00:00",
  updateDate: "2020-01-01 00:00:00",
  name: "Test Meal",
  description: "Test Meal Description",
  allergenes: "gluten"
};

const missingFieldMeal = {
  id: 1,
  isActive: 1,
  isVega: 0,
  isVegan: 0,
  isToTakeHome: 0,
  dateTime: "2022-03-22 17:35:00",
  maxAmountOfParticipants: 4,
  price: 10,
  imageUrl: "https://www.google.com/",
  cookId: 1,
  createDate: "2020-01-01 00:00:00",
  updateDate: "2020-01-01 00:00:00",
  description: "Test Meal Description",
  allergenes: "gluten"
};

describe("Participation", () => {
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_PARTICIPATION, (err, res) => {
      if (err) {
        done(err);
      }
      done();
    });
  });
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_MEAL, (err, res) => {
      if (err) {
        console.log(err);
        done(err);
      }
      done();
    });
  });
  beforeEach((done) => {
    pool.query(queries.CLEAR_DB_USER, (err, res) => {
      if (err) {
        done(err);
      }
      done();
    });
  });
  describe("UC401 Create Participation", () => {
    it("UT401-1 should return error, not authorized", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth("userToken", { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("UT401-2 should return error, meal doesnt exist", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth("userToken", { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  res.should.have.status(401);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.should.have.property("error");
                  obj.error.should.be.a("string");
                  obj.error.should.equal("Not authorized");
                  done();
                });
            });
        });
    });
    it("UT401-3 should create participation", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth(userToken, { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  res.should.have.status(200);
                  const obj = res.body;
                  obj.should.be.a("object");
                  obj.status.should.eql("success");
                  obj.should.have.property("result");
                  obj.result.should.be.a("object");
                  obj.result.should.have.property("insertId");
                  obj.result.insertId.should.be.a("number");
                  done();
                });
            });
        });
    });
  });
  describe("UC402 delete participation", () => {
    it("UT402-1 should return error, not authorized", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth(userToken, { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  chai
                    .request(server)
                    .delete("/api/participation")
                    .auth("userToken", { type: "bearer" })
                    .send(part)
                    .end((err, res) => {
                      res.should.have.status(401);
                      const obj = res.body;
                      obj.should.be.a("object");
                      obj.should.have.property("error");
                      obj.error.should.be.a("string");
                      obj.error.should.equal("Not authorized");
                      done();
                    });
                });
            });
        });
    });
    it("UT402-2 should return error, meal doesnt exist", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth(userToken, { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  chai
                    .request(server)
                    .delete("/api/participation")
                    .auth("userToken", { type: "bearer" })
                    .send(part)
                    .end((err, res) => {
                      res.should.have.status(401);
                      const obj = res.body;
                      obj.should.be.a("object");
                      obj.should.have.property("error");
                      obj.error.should.be.a("string");
                      obj.error.should.equal("Not authorized");
                      done();
                    });
                });
            });
        });
    });
    it("UT402-3 should delete participation", (done) => {
      let userToken;
      let id;
      chai
        .request(server)
        .post("/api/auth")
        .send(correctUser)
        .end((err, res) => {
          id = res.body.id;
          userToken = res.body.Token;
          let meal = {
            id: 1,
            isActive: 1,
            isVega: 0,
            isVegan: 0,
            isToTakeHome: 0,
            dateTime: "2022-03-22 17:35:00",
            maxAmountOfParticipants: 4,
            price: 10,
            imageUrl: "https://www.google.com/",
            cookId: id,
            createDate: "2020-01-01 00:00:00",
            updateDate: "2020-01-01 00:00:00",
            name: "Test Meal",
            description: "Test Meal Description",
            allergenes: "gluten"
          };
          chai
            .request(server)
            .post("/api/meal")
            .auth(userToken, { type: "bearer" })
            .send(meal)
            .end((err, res) => {
              const mealId = res.body.result.insertId;
              const part = {
                userId: String(id),
                mealId: String(mealId)
              };
              chai
                .request(server)
                .post("/api/participation")
                .auth(userToken, { type: "bearer" })
                .send(part)
                .end((err, res) => {
                  chai
                    .request(server)
                    .delete("/api/participation")
                    .auth(userToken, { type: "bearer" })
                    .send(part)
                    .end((err, res) => {
                      res.should.have.status(200);
                      const obj = res.body;
                      obj.should.be.a("object");
                      obj.should.have.property("result");
                      obj.status.should.eql("success");
                      obj.result.should.have.property("affectedRows");
                      obj.result.affectedRows.should.be.a("number");
                      obj.result.affectedRows.should.equal(1);
                      done();
                    });
                });
            });
        });
    });
  });
});
