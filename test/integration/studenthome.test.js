process.env.TEST_DB = process.env.TEST_DB || "studenthome_testdb";
process.env.TEST_DB_PASSWORD = process.env.TEST_DB_PASSWORD || "secret";
process.env.TEST_DB_HOST = process.env.TEST_DB_HOST || "localhost";
process.env.TEST_DB_USER = process.env.TEST_DB_USER || "root";
console.log(`Running tests using database '${process.env.TEST_DB}'`);

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../server");
const pool = require("../../src/dao/database").pool;
const queries = require("../../src/dao/queries");
const jwt = require("jsonwebtoken");
const id = process.env.TEST_ID;

chai.should();
chai.use(chaiHttp);

// describe("studenthome", () => {
//   before((done) => {
//     pool.query(queries.CLEAR_DB_HOME, (err, rows, fields) => {
//       if (err) {
//         console.log(`beforeEach CLEAR error: ${err}`);
//         done(err);
//       } else {
//         done();
//       }
//     });
//   });
//   describe("UC201 Create Home", () => {
//     it("TC-201-1 should throw an error when a field is missing", (done) => {
//       chai
//         .request(server)
//         .post("/api/studentHome/")
//         .set(
//           "authorization",
//           "Bearer " + jwt.sign({ id: id }, process.env.TOKEN_SECRET)
//         )
//         .send({
//           Address: "teststraat",
//           House_Nr: 23,
//           UserID: id,
//           Postal_Code: "1234AB",
//           Telephone: 0131234567,
//           City: "Breda"
//         })
//         .end((err, res) => {
//           res.should.have.status(400);
//           res.body.should.be
//             .an("object")
//             .that.has.all.keys("datetime", "message");
//           let { message } = res.body;
//           message.should.be
//             .an("string")
//             .that.equals(
//               "AssertionError [ERR_ASSERTION]: Name must be a string."
//             );
//           done();
//         });
//     });
//   });
//   it("TC-201-2 should throw an error when postal code is invalid", (done) => {
//     chai
//       .request(server)
//       .post("/api/studentHome/")
//       .set(
//         "authorization",
//         "Bearer " + jwt.sign({ id: id }, process.env.TOKEN_SECRET)
//       )
//       .send({
//         Name: "Test",
//         Address: "teststraat",
//         House_Nr: 2,
//         UserID: id,
//         Postal_Code: 1234,
//         Telephone: 0131234567,
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(400);
//         res.body.should.be
//           .an("object")
//           .that.has.all.keys("datetime", "message");
//         let { message } = res.body;
//         message.should.be
//           .an("string")
//           .that.equals(
//             "AssertionError [ERR_ASSERTION]: Postal_Code must be a string."
//           );
//         done();
//       });
//   });
//   it("TC-201-3 should throw an error when phone number is invalid", (done) => {
//     chai
//       .request(server)
//       .post("/api/studentHome/")
//       .set(
//         "authorization",
//         "Bearer " + jwt.sign({ id: id }, process.env.TOKEN_SECRET)
//       )
//       .send({
//         Name: "Test",
//         Address: "teststraat",
//         House_Nr: 2,
//         UserID: id,
//         Postal_Code: "1234AB",
//         Telephone: "0131234567",
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(400);
//         res.body.should.be
//           .an("object")
//           .that.has.all.keys("datetime", "message");
//         let { message } = res.body;
//         message.should.be
//           .an("string")
//           .that.equals(
//             "AssertionError [ERR_ASSERTION]: Telephone must be a number."
//           );
//         done();
//       });
//   });
//   it("TC-201-5 should throw an error when a user is not logged in", (done) => {
//     chai
//       .request(server)
//       .post("/api/studentHome/")
//       .send({
//         Name: "Test",
//         Address: "teststraat",
//         House_Nr: 2,
//         UserID: id,
//         Postal_Code: "1234AB",
//         Telephone: 0131234567,
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(401);
//         res.body.should.be.an("object").that.has.all.keys("error", "datetime");
//         let { error } = res.body;
//         error.should.be
//           .an("string")
//           .that.equals("Authorization header missing!");
//         done();
//       });
//   });
//   it("TC-201-6 should add studenthome if everyting is correct", (done) => {
//     chai
//       .request(server)
//       .post("/api/studentHome/")
//       .set(
//         "authorization",
//         "Bearer " + jwt.sign({ id: id }, process.env.TOKEN_SECRET)
//       )
//       .send({
//         Name: "Test",
//         Address: "teststraat",
//         House_Nr: 2,
//         UserID: id,
//         Postal_Code: "1234AB",
//         Telephone: 0131234567,
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(400);
//         res.body.should.be.an("object").that.has.all.keys("status", "result");
//         let { status } = res.body;
//         status.should.be
//           .an("string")
//           .that.equals(
//             "ER_DUP_ENTRY: Duplicate entry '4706RX-12' for key 'UniqueAdress'"
//           );
//         done();
//       });
//   });
//   it("TC-201-4 should throw an error when an address is in use", (done) => {
//     chai
//       .request(server)
//       .post("/api/studentHome/")
//       .set(
//         "authorization",
//         "Bearer " + jwt.sign({ id: id }, process.env.TOKEN_SECRET)
//       )
//       .send({
//         Name: "Test",
//         Address: "teststraat",
//         House_Nr: 2,
//         UserID: id,
//         Postal_Code: "1234AB",
//         Telephone: 0131234567,
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(400);
//         res.body.should.be
//           .an("object")
//           .that.has.all.keys("errCode", "error", "message");
//         let { error } = res.body;
//         error.should.be
//           .an("string")
//           .that.equals(
//             "ER_DUP_ENTRY: Duplicate entry '4706RX-12' for key 'UniqueAdress'"
//           );
//         done();
//       });
//   });
//   describe("UC202 Get Homes", () => {
//     it("TC-202-1 should show 0 homes", (done) => {
//       chai
//         .request(server)
//         .get("/api/studentHome/")
//         .send({})
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.body.should.be
//             .an("object")
//             .that.has.all.keys("status", "resultcount", "result");
//           let { resultcount } = 0;
//           resultcount.should.be.an("number").that.equals(0);
//           done();
//         });
//     });
//     it("TC-202-2 should show 2 homes", (done) => {
//       chai
//         .request(server)
//         .get("/api/studentHome/")
//         .send({})
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.body.should.be
//             .an("object")
//             .that.has.all.keys("status", "resultcount", "result");
//           let { resultcount } = 2;
//           resultcount.should.be.an("number").that.equals(2);
//           done();
//         });
//     });
//     it("TC-202-3 should throw error with wrong city", (done) => {
//       chai
//         .request(server)
//         .get("/api/studentHome/")
//         .send({
//           City: "Munchen"
//         })
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.body.should.be
//             .an("object")
//             .that.has.all.keys("status", "resultcount", "result");
//           let { resultcount } = res.body;
//           resultcount.should.be.an("number").that.equals(0);
//           done();
//         });
//     });
// });
// });
