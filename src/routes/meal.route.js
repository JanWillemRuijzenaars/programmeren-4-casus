const express = require("express");
const router = express.Router();

const MealController = require("../controllers/meal.controller");
const mealController = new MealController();
const AuthenticationController = require("../controllers/authentication.controller");
const authenticationController = new AuthenticationController();

router.post(
  "/",
  authenticationController.validateToken,
  mealController.validateMeal,
  mealController.create
);
router.put(
  "/:id",
  authenticationController.validateToken,
  mealController.validateMeal,
  mealController.update
);
router.get("/", mealController.getAll);
router.get("/:id", mealController.getOne);
router.delete(
  "/:id",
  authenticationController.validateToken,
  mealController.delete
);

module.exports = router;
