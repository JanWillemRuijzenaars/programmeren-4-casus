const express = require("express");
const router = express.Router();

const AuthenticationController = require("../controllers/authentication.controller");
const authenticationController = new AuthenticationController();

router.post("/login", authenticationController.validateLogin, authenticationController.login);
router.post("/", authenticationController.validateRegister, authenticationController.register);

module.exports = router;
