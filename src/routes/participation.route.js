const express = require("express");
const router = express.Router();

const ParticipationController = require("../controllers/participation.controller");
const participationController = new ParticipationController();
const AuthenticationController = require("../controllers/authentication.controller");
const authenticationController = new AuthenticationController();

router.post(
  "/",
  authenticationController.validateToken,
  participationController.validateParticipation,
  participationController.create
);
router.delete(
  "/",
  authenticationController.validateToken,
  participationController.delete
);
router.get(
  "/",
  authenticationController.validateToken,
  participationController.getAll
);
router.get(
  "/:id",
  authenticationController.validateToken,
  participationController.getOne
);

module.exports = router;
