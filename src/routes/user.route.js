const express = require("express");
const router = express.Router();

const UserController = require("../controllers/user.controller");
const userCrudController = new UserController();
const AuthenticationController = require("../controllers/authentication.controller");
const authenticationController = new AuthenticationController();

router.get("/", userCrudController.getAll);
router.get("/:id", authenticationController.validateToken, userCrudController.getOne);
router.get("/own/:id", authenticationController.validateToken, userCrudController.getOwn);
router.put("/:id", authenticationController.validateToken, userCrudController.validateUser, userCrudController.update);
router.delete("/:id", authenticationController.validateToken, userCrudController.delete);

module.exports = router;
