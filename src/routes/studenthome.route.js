const express = require("express");
const router = express.Router();

const StudentHomeController = require("../controllers/studenthome.controller");
const studentHomeCrudController = new StudentHomeController();
const AuthenticationController = require("../controllers/authentication.controller");
const authenticationController = new AuthenticationController();

router.post(
  "/",
  studentHomeCrudController.validateHome,
  studentHomeCrudController.create
);
router.get("/", studentHomeCrudController.getAll);
router.get("/:id", studentHomeCrudController.getOne);
router.put(
  "/:id",
  studentHomeCrudController.validateHome,
  studentHomeCrudController.update
);
router.delete(
  "/:id",
  studentHomeCrudController.delete
);

module.exports = router;
