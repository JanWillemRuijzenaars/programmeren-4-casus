const config = require("../config");
const queries = require("./queries");
const db = require("./database");
const logger = config.logger;

const participationDao = {
  create: (participation, callback) => {
    logger.debug("create", participation);

    let { mealId, userId } = participation;

    db.query(
      queries.PARTICIPATION_CREATE,
      [mealId, userId],
      (err, results) => {
        if (err) {
          logger.trace("create", err);
          callback(err, undefined);
        }
        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  delete: (participation, callback) => {
    logger.debug("delete");
    let { mealId, userId } = participation;

    db.query(
      queries.PARTICIPATION_DELETE,
      [mealId, userId],
      (err, results) => {
        if (err) {
          logger.trace("delete", err);
          callback(err, undefined);
        }

        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  getAll: (body, callback) => {
    logger.debug("getAll, body:", body);
    db.query(queries.PARTICIPATION_SELECT_ALL, [body.mealId], (err, results) => {
      if (err) {
        logger.trace("getAll", err);
        callback(err, null);
      }
      if (results) {
        logger.debug("results: ", results);
        callback(null, results);
      }
    });
  },

  getOne: (id, callback) => {
    logger.debug("getOne");

    db.query(queries.PARTICIPATION_SELECT_ONE, [id], (err, results) => {
      if (err) {
        logger.trace("getOne", err);
        callback(err, null);
      }

      if (results) {
        callback(null, results[0]);
      }
    });
  }
};

module.exports = participationDao;
