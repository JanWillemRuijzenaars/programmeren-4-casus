const config = require("../config");
const queries = require("./queries");
const db = require("./database");
const logger = config.logger;

const mealDao = {
  create: (meal, callback) => {
    logger.debug("create", meal);

    let {
      isActive,
      isVega,
      isVegan,
      isToTakeHome,
      dateTime,
      maxAmountOfParticipants,
      price,
      imageUrl,
      cookId,
      createDate,
      updateDate,
      name,
      description,
      allergenes
    } = meal;

    db.query(
      queries.MEAL_CREATE,
      [
        isActive,
        isVega,
        isVegan,
        isToTakeHome,
        dateTime,
        maxAmountOfParticipants,
        price,
        imageUrl,
        cookId,
        createDate,
        updateDate,
        name,
        description,
        allergenes
      ],
      (err, results) => {
        if (err) {
          logger.trace("create", err);
          callback(err, undefined);
        }
        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  getAll: (callback) => {
    logger.debug("getAll");

    db.query(queries.MEAL_SELECT_All , [], (err, results) => {
      if (err) {
        logger.trace("getAll", err);
        callback(err, undefined);
      }
      if (results) {
        logger.debug("results: ", results);
        callback(undefined, results);
      }
    });
  },

  getOne: (id, callback) => {
    logger.debug("getOne");

    db.query(queries.MEAL_SELECT_ONE, [id], (err, results) => {
      if (err) {
        logger.trace("getOne", err);
        callback(err, undefined);
      }

      if (results) {
        callback(undefined, results[0]);
      }
    });
  },
  update: (meal, id, callback) => {
    logger.debug("update");
    let {
      isActive,
      isVega,
      isVegan,
      isToTakeHome,
      dateTime,
      maxAmountOfParticipants,
      price,
      imageUrl,
      cookId,
      createDate,
      updateDate,
      name,
      description,
      allergenes,
    } = meal;
    db.query(
      queries.MEAL_UPDATE,
      [
        isActive,
        isVega,
        isVegan,
        isToTakeHome,
        dateTime,
        maxAmountOfParticipants,
        price,
        imageUrl,
        cookId,
        createDate,
        updateDate,
        name,
        description,
        allergenes,
        id
      ],
      (err, results) => {
        if (err) {
          logger.trace("update", err);
          callback(err, undefined);
        }

        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  delete: (id, callback) => {
    logger.debug("delete");

    db.query(queries.MEAL_DELETE, [id], (err, results) => {
      if (err) {
        logger.trace("delete", err);
        callback(err, undefined);
      }

      if (results) {
        callback(undefined, results);
      }
    });
  }
};
module.exports = mealDao;
