const config = require("../config");
const queries = require("./queries");
const db = require("./database");
const logger = config.logger;

const userDao = {
  getAll: (callback) => {
    logger.debug("getAll");

    db.query(queries.USER_SELECT_ALL, (err, results) => {
      if (err) {
        logger.trace("getAll", err);
        callback(err, null);
      }
      if (results) {
        logger.debug("results: ", results);
        callback(null, results);
      }
    });
  },

  getOwn: (id, callback) => {
    logger.debug("getOwn");

    db.query(queries.USER_SELECT_OWN_PROFILE, [id], (err, results) => {
      if (err) {
        logger.trace("getOwn", err);
        callback(err, null);
      }

      if (results) {
        callback(null, results[0]);
      }
    });
  },

  getOne: (id, callback) => {
    logger.debug("getOne");

    db.query(queries.USER_SELECT_ONE, [id], (err, results) => {
      if (err) {
        logger.trace("getOne", err);
        callback(err, null);
      }

      if(!results) {
        callback(null, results[0]);
      }

      if (results) {
        callback(null, results[0]);
      }
    });
  },
  update: (user, id, callback) => {
    logger.debug("update");
    let {
      firstName,
      lastName,
      isActive,
      emailAdress,
      password,
      phoneNumber,
      roles,
      street,
      city
    } = user;
    db.query(
      queries.USER_UPDATE,
      [
        firstName,
        lastName,
        isActive,
        emailAdress,
        password,
        phoneNumber,
        roles,
        street,
        city
      ],
      (err, results) => {
        if (err) {
          logger.trace("update", err);
          callback(err, undefined);
        }

        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  delete: (id, callback) => {
    logger.debug("delete");

    db.query(queries.USER_DELETE, [id], (err, results) => {
      if (err) {
        logger.trace("delete", err);
        callback(err, undefined);
      }

      if (results) {
        callback(undefined, results);
      }
    });
  }
};
module.exports = userDao;
