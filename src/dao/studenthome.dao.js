const config = require("../config");
const queries = require("./queries");
const db = require("./database");
const logger = config.logger;

const studentHomeDao = {
  create: (home, callback) => {
    logger.debug("create", home);

    let {
      Name,
      Address,
      House_Nr,
      UserID,
      Postal_Code,
      Telephone,
      City
    } = home;

    db.query(
      queries.STUDENTHOME_CREATE,
      [Name, Address, House_Nr, UserID, Postal_Code, Telephone, City],
      (err, results) => {
        if (err) {
          logger.trace("create", err);
          callback(err, undefined);
        }
        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  getAll: (body, callback) => {
    logger.debug("getAll");
    if (body.Name != null || body.Name != undefined) {
      const Name = body.Name + "%";
      db.query(
        queries.STUDENTHOME_SELECT_ALL_BY_NAME,
        [Name],
        (err, results) => {
          if (err) {
            logger.trace("getAll", err);
            callback(err, null);
          }
          if (results) {
            logger.debug("results: ", results);
            callback(null, results);
          }
        }
      );
    } else if (body.City != null || body.City != undefined) {
      db.query(
        queries.STUDENTHOME_SELECT_ALL_BY_CITY,
        [body.City],
        (err, results) => {
          if (err) {
            logger.trace("getAll", err);
            callback(err, null);
          }
          if (results) {
            logger.debug("results: ", results);
            callback(null, results);
          }
        }
      );
    } else {
      db.query(queries.STUDENTHOME_SELECT_ALL, (err, results) => {
        if (err) {
          logger.trace("getAll", err);
          callback(err, null);
        }
        if (results) {
          logger.debug("results: ", results);
          callback(null, results);
        }
      });
    }
  },

  getOne: (id, callback) => {
    logger.debug("getOne");

    db.query(queries.STUDENTHOME_SELECT_ONE, [id], (err, results) => {
      if (err) {
        logger.trace("getOne", err);
        callback(err, null);
      }

      if (results) {
        callback(null, results[0]);
      }
    });
  },
  update: (home, id, callback) => {
    logger.debug("update");
    let { Name, Address, House_Nr, Postal_Code, Telephone, City } = home;
    db.query(
      queries.STUDENTHOME_UPDATE,
      [Name, Address, House_Nr, Postal_Code, Telephone, City, id],
      (err, results) => {
        if (err) {
          logger.trace("update", err);
          callback(err, undefined);
        }

        if (results) {
          callback(undefined, results);
        }
      }
    );
  },
  delete: (id, callback) => {
    logger.debug("delete");

    db.query(queries.STUDENTHOME_DELETE, [id], (err, results) => {
      if (err) {
        logger.trace("delete", err);
        callback(err, undefined);
      }

      if (results) {
        callback(undefined, results);
      }
    });
  }
};
module.exports = studentHomeDao;
