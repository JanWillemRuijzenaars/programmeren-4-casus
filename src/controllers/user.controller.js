const logger = require("../config").logger;
const userDao = require("../dao/user.dao");
const assert = require("assert");
const Utils = require("../utils");
const utils = new Utils();

class UserController {
  constructor() {}

  validateUser = async (req, res, next) => {
    try {
      assert(
        typeof req.body.firstName === "string",
        "firstName must be a string."
      );
      assert(
        typeof req.body.lastName === "string",
        "lastName must be a string."
      );
      assert(
        typeof req.body.emailAdress === "string",
        "emailAdress must be a string."
      );
      assert(
        typeof req.body.password === "string",
        "password must be a string."
      );
      assert(
        typeof req.body.phoneNumber === "string",
        "phoneNumber must be a string."
      );
      assert(typeof req.body.roles === "string", "roles must be a string.");
      assert(typeof req.body.street === "string", "street must be a string.");
      assert(typeof req.body.city === "string", "city must be a string.");
      next();
    } catch (ex) {
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  getAll = async (req, res, next) => {
    logger.debug("UserController getAll called");
    userDao.getAll((err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getOne = async (req, res, next) => {
    logger.debug("UserController getOne called");
    userDao.getOne(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getOwn = async (req, res, next) => {
    logger.debug("UserController getOwn called");
    userDao.getOwn(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  update = async (req, res, next) => {
    logger.debug("UserController update called");
    const user = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      isActive: req.body.isActive,
      emailAdress: req.body.emailAdress,
      password: req.body.password,
      phoneNumber: req.body.phoneNumber,
      roles: req.body.roles,
      street: req.body.street,
      city: req.body.city
    };
    userDao.update(user, req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  delete = async (req, res, next) => {
    logger.debug("UserController delete called");
    userDao.delete(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
}
module.exports = UserController;
