const logger = require("../config").logger;
const participationDao = require("../dao/participation.dao");
const Utils = require("../utils");
const assert = require("assert");
const utils = new Utils();

class ParticipationController {
  constructor() {}

  validateParticipation = async (req, res, next) => {
    try {
      assert(typeof req.body.mealId === "string", "mealId must be a string.");
      assert(
        typeof req.body.userId === "string",
        "userId must be a string."
      );
      next();
    } catch (ex) {
      res
        .status(422)
        .json({ error: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  create = async (req, res, next) => {
    logger.debug("ParticipationController create called");
    participationDao.create(req.body, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  delete = async (req, res, next) => {
    logger.debug("ParticipationController delete called");
    participationDao.delete(req.body, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getAll = async (req, res, next) => {
    logger.debug("ParticipationController getAll called");
    participationDao.getAll(req.body, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getOne = async (req, res, next) => {
    logger.debug("ParticipationController getOne called");
    participationDao.getOne(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
}
module.exports = ParticipationController;
