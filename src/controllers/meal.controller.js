const logger = require("../config").logger;
const mealDao = require("../dao/meal.dao");
const assert = require("assert");
const Utils = require("../utils");
const utils = new Utils();

function getCompleteMeal(rawMeal) {
  const meal = {
    isActive: rawMeal.isActive,
    isVega: rawMeal.isVega,
    isVegan: rawMeal.isVegan,
    isToTakeHome: rawMeal.isToTakeHome,
    dateTime: rawMeal.dateTime,
    maxAmountOfParticipants: rawMeal.maxAmountOfParticipants,
    price: rawMeal.price,
    imageUrl: rawMeal.imageUrl,
    cookId: rawMeal.cookId,
    createDate: rawMeal.createDate,
    updateDate: rawMeal.updateDate,
    name: rawMeal.name,
    description: rawMeal.description,
    allergenes: rawMeal.allergenes
  };
  return meal;
}

class MealController {
  constructor() {}

  validateMeal = async (req, res, next) => {
    try {
      assert(typeof req.body.dateTime === "string", "dateTime must be a date.");

      assert(
        typeof req.body.maxAmountOfParticipants === "number",
        "maxAmountOfParticipants must be a number."
      );
      assert(typeof req.body.price === "number", "price must be a number.");
      assert(
        typeof req.body.imageUrl === "string",
        "imageUrl must be a string."
      );
      assert(typeof req.body.cookId === "number", "cookId must be a number.");
      assert(
        typeof req.body.createDate === "string",
        "createDate must be a date."
      );
      assert(
        typeof req.body.updateDate === "string",
        "updateDate must be a date."
      );
      assert(typeof req.body.name === "string", "name must be a string.");
      assert(
        typeof req.body.description === "string",
        "description must be a string."
      );
      assert(
        typeof req.body.allergenes === "string",
        "allergenes must be a string."
      );
      next();
    } catch (ex) {
      res
        .status(422)
        .json({ error: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  create = async (req, res, next) => {
    logger.debug("MealController create called");
    const meal = getCompleteMeal(req.body);
    mealDao.create(meal, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };

  getAll = async (req, res, next) => {
    logger.debug("MealController getAll called", req.body);
    mealDao.getAll((err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getOne = async (req, res, next) => {
    logger.debug("MealController getOne called");
    mealDao.getOne(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  update = async (req, res, next) => {
    logger.debug("MealController update called");
    const meal = getCompleteMeal(req.body);
    mealDao.update(meal, req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  delete = async (req, res, next) => {
    logger.debug("MealController delete called");
    mealDao.delete(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
}
module.exports = MealController;
