const logger = require("../config").logger;
const studentHomeDao = require("../dao/studenthome.dao");
const assert = require("assert");
const Utils = require("../utils");
const utils = new Utils();

class StudentHomeController {
  constructor() {}

  validateHome = async (req, res, next) => {
    try {
      assert(typeof req.body.Name === "string", "Name must be a string.");
      assert(typeof req.body.Address === "string", "Address must be a string.");
      assert(
        typeof req.body.House_Nr === "number",
        "House_Nr must be a number."
      );
      assert(
        typeof req.body.Postal_Code === "string",
        "Postal_Code must be a string."
      );
      assert(
        typeof req.body.Telephone === "number",
        "Telephone must be a number."
      );
      assert(typeof req.body.City === "string", "City must be a string.");
      next();
    } catch (ex) {
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  create = async (req, res, next) => {
    logger.debug("StudenHomeController create called");
    studentHomeDao.create(req.body, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getAll = async (req, res, next) => {
    logger.debug("StudenHomeController getAll called");
    studentHomeDao.getAll(req.body, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  getOne = async (req, res, next) => {
    logger.debug("StudenHomeController getOne called");
    studentHomeDao.getOne(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  update = async (req, res, next) => {
    logger.debug("StudenHomeController update called");
    const home = {
      Name: req.body.Name,
      Address: req.body.Address,
      House_Nr: req.body.House_Nr,
      Postal_Code: req.body.Postal_Code,
      Telephone: req.body.Telephone,
      City: req.body.City
    };
    studentHomeDao.update(home, req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
  delete = async (req, res, next) => {
    logger.debug("StudenHomeController delete called");
    studentHomeDao.delete(req.params.id, (err, result) => {
      utils.handleResult(res, next, err, result);
    });
  };
}
module.exports = StudentHomeController;
