var logger = require("../config").logger;
const queries = require("../dao/queries");
const assert = require("assert");
const jwt = require("jsonwebtoken");
const pool = require("../dao/database").pool;
const jwtSecretKey = process.env.TOKEN_SECRET;

class AuthenticationController {
  constructor() {}
  login = async (req, res, next) => {
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        connection.query(
          queries.USER_LOGIN,
          [req.body.emailAdress],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString()
              });
            } else {
              logger.info("Result from database: ", rows);
              if (
                rows &&
                rows.length === 1 &&
                rows[0].password == req.body.password
              ) {
                logger.info("passwords DID match, sending valid token");
                const payload = {
                  id: rows[0].id
                };
                const userinfo = {
                  id: rows[0].id,
                  firstName: rows[0].firstName,
                  lastName: rows[0].lastName,
                  emailAdress: rows[0].emailAdress,
                  phoneNumber: rows[0].phoneNumber,
                  roles: rows[0].roles,
                  street: rows[0].street,
                  city: rows[0].city,
                  Token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" })
                };
                logger.debug("Logged in, sending: ", userinfo);
                res.status(200).json(userinfo);
              } else {
                logger.info("User not found or password invalid");
                res.status(401).json({
                  message: "User not found or password invalid",
                  datetime: new Date().toISOString()
                });
              }
            }
          }
        );
      }
    });
  };

  validateLogin = async (req, res, next) => {
    try {
      assert(
        typeof req.body.emailAdress === "string",
        "emailAdress must be a string."
      );
      assert(
        typeof req.body.password === "string",
        "password must be a string."
      );
      next();
    } catch (ex) {
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  register = async (req, res, next) => {
    logger.info("register");
    logger.info(req.body);

    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool: " + err.toString());
        res
          .status(500)
          .json({ error: ex.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        let {
          firstName,
          lastName,
          isActive,
          emailAdress,
          password,
          phoneNumber,
          roles,
          street,
          city
        } = req.body;

        connection.query(
          queries.USER_CREATE,
          [
            firstName,
            lastName,
            isActive,
            emailAdress,
            password,
            phoneNumber,
            roles,
            street,
            city
          ],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: " + err.toString());
              res.status(400).json({
                message: "This email has already been taken.",
                datetime: new Date().toISOString()
              });
            } else {
              logger.trace(rows);
              const payload = {
                id: rows.insertId
              };
              const userinfo = {
                id: rows.insertId,
                firstName: firstName,
                lastName: lastName,
                emailAdress: emailAdress,
                password: password,
                phoneNumber: phoneNumber,
                roles: roles,
                street: street,
                city: city,
                Token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" })
              };
              logger.debug("Registered", userinfo);
              res.status(200).json(userinfo);
            }
          }
        );
      }
    });
  };

  validateRegister = async (req, res, next) => {
    try {
      assert(
        typeof req.body.firstName === "string",
        "firstName must be a string."
      );
      assert(
        typeof req.body.lastName === "string",
        "lastName must be a string."
      );
      assert(
        typeof req.body.isActive === "number",
        "isActive must be a number."
      )
      assert(
        typeof req.body.emailAdress === "string",
        "emailAdress must be a string."
      );
      assert(
        typeof req.body.password === "string",
        "password must be a string."
      );
      assert(
        typeof req.body.phoneNumber === "string",
        "phoneNumber must be a string."
      );
      assert(
        typeof req.body.roles === "string",
        "roles must be a string."
      );
      assert(
        typeof req.body.street === "string",
        "street must be a string."
      );
      assert(
        typeof req.body.city === "string",
        "city must be a string."
      );
      next();
    } catch (ex) {
      logger.debug("validateRegister error: ", ex.toString());
      res
        .status(400)
        .json({ message: ex.toString(), datetime: new Date().toISOString() });
    }
  };

  validateToken = async (req, res, next) => {
    logger.info("validateToken called");
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      res.status(401).json({
        error: "Authorization header missing!",
        datetime: new Date().toISOString()
      });
    } else {
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          res.status(401).json({
            error: "Not authorized",
            datetime: new Date().toISOString()
          });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          req.UserID = payload.id;
          next();
        }
      });
    }
  };

  renewToken = async (req, res, next) => {
    logger.debug("renewToken");

    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        connection.query(
          queries.TOKEN_RENEW,
          [req.UserID],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString()
              });
            } else {
              const payload = {
                id: rows[0].id
              };
              const userinfo = {
                id: rows[0].id,
                firstName: rows[0].firstName,
                lastName: rows[0].lastName,
                emailAdress: rows[0].emailAdress,
                phoneNumber: rows[0].phoneNumber,
                roles: rows[0].roles,
                street: rows[0].street,
                city: rows[0].city,
                Token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" })
              };
              logger.debug("Sending: ", userinfo);
              res.status(200).json(userinfo);
            }
          }
        );
      }
    });
  };
}

module.exports = AuthenticationController;
